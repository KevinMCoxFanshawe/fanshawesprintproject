/**
 * Project:       Sprint Tool Project
 * School:        Fanshawe College
 * Class:         INFO3112
 * Semester:      Winter 2019
 * Professor:     Admiral Brian Turford
 * Team Members:  Ryan Augustynowicz, Kevin Cox, Nomaan Abbasey
 */

import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import VelocityIcon from "@material-ui/icons/Timeline";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import LibraryAddIcon from "@material-ui/icons/LibraryAdd";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import DoneIcon from "@material-ui/icons/Done";

export default class Crud {
  constructor(component) {
    this.component = component;
  }

  loadAllDocuments = async (showMessage = false) => {
    var data = await this.getAllDocuments(
      this.component.state.datatype,
      showMessage
    );
    let newVelocity = 0;
    data.map(index => {
      if (parseInt(index["completion_percentage"]) === 100) {
        newVelocity += +index["priority"];
      }
    });
    this.component.setState({ velocity: newVelocity });
    this.component.setState({ data: data, selected: [] });
  };

  getAllDocuments = async (datatype, showMessage = false) => {
    //get the info from the database
    try {
      let response = await fetch(datatype.endpoint + "/all");
      let jsonResponce = await response.json();

      if (showMessage) {
        this.component.props.loadMessage(
          `Loaded ${jsonResponce.length} ${datatype.name}(s)!`
        );
      }

      return jsonResponce;
    } catch (error) {
      this.component.props.loadMessage(
        `Error: Unable to load ${datatype.name}(s) from database ${error}`
      );
      return [];
    }
  };

  deleteSelectedDocuments = async () => {
    try {
      let jsonResponce;
      for (let i = 0; i < this.component.state.selected.length; i++) {
        let response = await fetch(
          this.component.state.datatype.endpoint +
          "/" +
          this.component.state.selected[i],
          {
            method: "DELETE",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              objectsToDelete: this.component.state.selected
            })
          }
        );

        jsonResponce = await response.json();
      }

      this.component.props.loadMessage(`${jsonResponce}`);
    } catch (error) {
      console.log(error);
      this.component.props.loadMessage(
        `Error: Unable to delete ${
        this.component.state.datatype.name
        } from database ${error}`
      );
    }

    //call database update. make sure we have an up to date table
    this.loadAllDocuments();
    this.closeDialog();
  };

  updateOpenedDocument = async () => {
    var passedObject = this.component.state.openDocument;
    passedObject["_id"] = this.component.state.selected[0];
    delete passedObject["title"];
    delete passedObject["updateItem"];

    // ensure that all boolean values are converted from string to bool
    // ex: "true" -> true
    this.component.state.datatype.fields.forEach(field => {
      if (field.type === "checkbox") {
        passedObject[field.id] =
          passedObject[field.id] === "true" || passedObject[field.id] === true;
      }
    });

    try {
      let response = await fetch(this.component.state.datatype.endpoint, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(passedObject)
      });

      let jsonResponce = await response.json();

      this.component.props.loadMessage(` ${jsonResponce.msg} `);
    } catch (error) {
      console.log(error);
      this.component.loadMessage(
        `Error: Unable to update ${
        this.component.state.datatype.name_pluralized
        } in database`
      );
    }

    this.loadAllDocuments();
    this.closeDialog();
  };

  updateCompleteValue = () => {
    var newItemInfo = this.component.state.openDocument;
    newItemInfo["completion_percentage"] = 100;
    this.component.setState({ openDocument: newItemInfo });
    console.log(this.component.state.openDocument);
    this.updateOpenedDocument();
  };

  createNewDocument = async () => {
    var tempPassed = this.component.state.openDocument;
    delete tempPassed["title"];
    delete tempPassed["updateItem"];
    try {
      let response = await fetch(this.component.state.datatype.endpoint, {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(this.component.state.openDocument)
      });

      await response.json();

      this.component.props.loadMessage(
        `Added New ${this.component.state.datatype.name_pluralized}`
      );
      this.closeDialog();
    } catch (error) {
      console.log(error);
      this.component.props.loadMessage(
        `Error: Unable to add ${
        this.component.state.datatype.name
        } to the database ${error}`
      );
    }

    this.loadAllDocuments();
  };

  openCreateDocumentDialog = () => {
    if (typeof this.component.refreshData === "function") {
      this.component.refreshData();
    }

    this.component.setState({
      updateDialogVisible: true,
      openDocument: {
        title: `Add ${this.component.state.datatype.name}`,
        updateItem: false
      }
    });
  };

  openDeleteDocumentDialog = () => {
    this.component.setState({ deleteDialogVisible: true });
  };

  openUpdateDocumentDialog = async () => {
    if (typeof this.component.refreshData === "function") {
      this.component.refreshData();
    }

    try {
      let response = await fetch(
        this.component.state.datatype.endpoint +
        "/" +
        this.component.state.selected[0]
      );
      let jsonResponce = await response.json();
      jsonResponce = jsonResponce[0];

      var openDocument = {
        title: `Edit ${this.component.state.datatype.name}`,
        updateItem: true
      };
      this.component.state.datatype.fields.forEach(field => {
        openDocument[field.id] = jsonResponce[field.id];
      });
      this.component.setState({
        updateDialogVisible: true,
        openDocument: openDocument
      });
    } catch (error) {
      this.component.props.loadMessage(
        `Error: Unable to load ${
        this.component.state.datatype.name
        } from database ${error}`
      );
      this.closeDialog();
    }
  };

  closeDialog = () => {
    this.component.setState({
      deleteDialogVisible: false,
      updateDialogVisible: false
    });
  };

  handleClick = (event, id) => {
    const { selected } = this.component.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    this.component.setState({ selected: newSelected });
  };

  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.component.setState(state => ({
        selected: this.component.state.data.map(n => n._id)
      }));
      return;
    }
    this.component.setState({ selected: [] });
  };

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";

    if (
      this.component.state.orderBy === property &&
      this.component.state.order === "desc"
    ) {
      order = "asc";
    }

    this.component.setState({ order, orderBy });
  };

  // drop downs are different b/c component in component
  updateDropDown = (event, item) => {
    var newItemInfo = this.component.state.openDocument;

    newItemInfo[item.props.id] = event.target.value;

    if (item.props.id === "story") {
      //find the id

      for (var i = 0; i < this.component.state.userStories.length; i++) {
        if (
          this.component.state.userStories[i].story === newItemInfo["story"]
        ) {
          newItemInfo["estimate"] = this.component.state.userStories[
            i
          ].estimate;
        }
      }
    }

    this.component.setState({ openDocument: newItemInfo });
  };

  processFormInput = event => {
    let value = event.target.value;

    // support for checkboxes
    this.component.state.datatype.fields.forEach(field => {
      if (field.id === event.target.id && field.type === "checkbox") {
        value = event.target.checked;
      }
    });

    var newItemInfo = this.component.state.openDocument;
    newItemInfo[event.target.id] = value;
    this.component.setState({ openDocument: newItemInfo });
    console.log(
      "open document state: " +
      JSON.stringify(this.component.state.openDocument)
    );
  };

  handleChangeRowsPerPage = event => {
    this.component.setState({ rowsPerPage: event.target.value });
  };

  handleChangePage = (event, page) => {
    this.component.setState({ page });
  };

  isSelected = id => this.component.state.selected.indexOf(id) !== -1;

  createFields = () => {
    let fields = [];

    this.component.state.datatype.fields.forEach(field => {
      if (field.render === false) {
        return; // skips this field
      }

      if (field.type === "checkbox") {
        fields.push(
          <FormControlLabel
            fullWidth
            control={
              <Checkbox
                id={field.id}
                label={field.label || field.id}
                type={field.type || "string"}
                defaultValue={field.default || ""}
                value={this.component.state.openDocument[field.id]}
                defaultChecked={
                  field.type === "checkbox"
                    ? this.component.state.openDocument[field.id] === true
                    : true
                }
                onChange={this.processFormInput}
                onClick={this.processFormInput}
              />
            }
            label={field.label || field.id}
          />
        );
      } else if (
        field.label !== "Sprint - User Story" &&
        field.label !== "Sprint - Relative Estimate"
      ) {
        fields.push(
          <TextField
            fullWidth
            id={field.id}
            label={field.label || field.id}
            type={field.type || "string"}
            defaultValue={field.default || ""}
            value={this.component.state.openDocument[field.id]}
            defaultChecked={
              field.type === "checkbox"
                ? this.component.state.openDocument[field.id] === true
                : true
            }
            onChange={this.processFormInput}
            onClick={this.processFormInput}
          />
        );
      }
    });

    return fields;
  };

  renderDocumentRow = doc => {
    let rows = [];
    this.component.state.datatype.fields.forEach(field => {
      if (field.renderColumn === false) {
        return; // do not render this row
      }
      rows.push(<TableCell align="right"> {doc[field.id]}</TableCell>);
    });
    return rows;
  };

  render = (classes, flag) => {
    const {
      data,
      order,
      orderBy,
      selected,
      rowsPerPage,
      page,
      updateDialogVisible,
      deleteDialogVisible,
      openDocument,
      datatype,
      velocity
    } = this.component.state;
    const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);
    return (
      <Paper className={classes.root}>
        <Dialog
          open={deleteDialogVisible}
          onClose={this.closeDialog}
          aria-labelledby="alert-dialog-delete-title"
          aria-describedby="alert-dialog-delete-description"
        >
          <DialogTitle id="alert-dialog-delete-title">{`Are you sure?`}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-delete-description">
              Are you sure you want to delete {selected.length}{" "}
              {this.component.state.datatype.name.toLowerCase()}(s)?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.deleteSelectedDocuments} color="primary">
              Yes
            </Button>
            <Button onClick={this.closeDialog} color="primary" autoFocus>
              No
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          open={updateDialogVisible}
          onClose={this.closeDialog}
          aria-labelledby="form-dialog-open-item"
        >
          <DialogTitle id="form-dialog-open-item">
            {openDocument["title"]}
          </DialogTitle>
          <DialogContent>
            {this.createFields()}
            {typeof (this.component.renderSpecialFields === "function")
              ? this.component.renderSpecialFields()
              : []}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeDialog} color="primary">
              Cancel
            </Button>
            {openDocument["updateItem"] && (
              <div>
                <Button onClick={this.updateOpenedDocument} color="primary">
                  Update {datatype.name}
                </Button>
                <Tooltip title="Complete Story">
                  <IconButton
                    aria-label="Complete"
                    onClick={() => {
                      this.updateCompleteValue();
                    }}
                  >
                    <DoneIcon />
                  </IconButton>
                </Tooltip>
              </div>
            )}
            {!openDocument["updateItem"] && (
              <Button onClick={this.createNewDocument} color="primary">
                Add {datatype.name}
              </Button>
            )}
          </DialogActions>
        </Dialog>

        <Toolbar
          className={classNames(classes.root, {
            [classes.highlight]: selected.length > 0
          })}
        >
          <div className={classes.title}>
            {selected.length > 0 ? (
              <Typography color="inherit" variant="subtitle1">
                {selected.length} selected
              </Typography>
            ) : (
                <div>
                  <Typography variant="h6" id="tableTitle">
                    {datatype.name_pluralized}
                  </Typography>
                  {flag && (
                    <Typography variant="body1" id="velocity">
                      <Tooltip title="Velocity">
                        <VelocityIcon />
                      </Tooltip>
                      {velocity}
                    </Typography>
                  )}
                </div>
              )}
          </div>
          <div className={classes.spacer} />
          <div className={classes.actions}>
            {selected.length === 1 && (
              <Toolbar>
                <Tooltip title="Delete">
                  <IconButton
                    aria-label="Delete"
                    onClick={() => {
                      this.openDeleteDocumentDialog();
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Edit">
                  <IconButton
                    aria-label="Edit"
                    onClick={() => {
                      this.openUpdateDocumentDialog();
                    }}
                  >
                    <EditIcon />
                  </IconButton>
                </Tooltip>
              </Toolbar>
            )}
            {selected.length > 1 && (
              <Toolbar>
                <Tooltip title="Delete">
                  <IconButton
                    aria-label="Delete"
                    onClick={() => {
                      this.openDeleteDocumentDialog();
                    }}
                  >
                    <DeleteIcon />
                  </IconButton>
                </Tooltip>
              </Toolbar>
            )}
            {selected.length === 0 && (
              <Toolbar>
                <Tooltip title={`Add ${datatype.name}`}>
                  <IconButton
                    aria-label={`Add ${datatype.name}`}
                    onClick={() => {
                      this.openCreateDocumentDialog();
                    }}
                  >
                    <LibraryAddIcon />
                  </IconButton>
                </Tooltip>
              </Toolbar>
            )}
          </div>
        </Toolbar>
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <DocumentTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
              fields={datatype.fields}
            />
            <TableBody>
              {stableSort(data, getSorting(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(n => {
                  const isSelected = this.isSelected(n._id);

                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, n._id)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n._id}
                      selected={isSelected}
                      style={
                        this.component.getDocumentRowColor(n) === "white"
                          ? {}
                          : {
                            background: this.component.getDocumentRowColor(n)
                          }
                      }
                    >
                      <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                      </TableCell>
                      {this.renderDocumentRow(n)}
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            "aria-label": "Previous Page"
          }}
          nextIconButtonProps={{
            "aria-label": "Next Page"
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  };
}

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
  return order === "desc"
    ? (a, b) => desc(a, b, orderBy)
    : (a, b) => -desc(a, b, orderBy);
}

class DocumentTableHead extends React.Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount,
      fields
    } = this.props;

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {fields.map(
            row =>
              row.renderColumn === false ? (
                []
              ) : (
                  <TableCell
                    key={row.id}
                    align={row.numeric ? "right" : "left"}
                    padding={row.disablePadding ? "none" : "default"}
                    sortDirection={orderBy === row.id ? order : false}
                  >
                    <Tooltip
                      title="Sort"
                      placement={row.numeric ? "bottom-end" : "bottom-start"}
                      enterDelay={300}
                    >
                      <TableSortLabel
                        active={orderBy === row.id}
                        direction={order}
                        onClick={this.createSortHandler(row.id)}
                      >
                        {row.label}
                      </TableSortLabel>
                    </Tooltip>
                  </TableCell>
                ),
            this
          )}
        </TableRow>
      </TableHead>
    );
  }
}

DocumentTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};