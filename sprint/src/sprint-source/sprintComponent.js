/**
 * Project:       Sprint Tool Project
 * School:        Fanshawe College
 * Class:         INFO3112
 * Semester:      Winter 2019
 * Professor:     Admiral Brian Turford
 * Team Members:  Ryan Augustynowicz, Kevin Cox, Nomaan Abbasey
 */

import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Crud from "./crud";
import { TextField } from "@material-ui/core";

const urls = require("./urls");

class SprintItemTable extends React.Component {
  state = {
    order: "asc",
    orderBy: "desc",
    selected: [],
    data: [],
    page: 0,
    rowsPerPage: 5,
    deleteDialogVisible: false,
    openDocument: {},
    updateDialogVisible: false,
    userStories: [],
    projectMembers: [],
    crud: new Crud(this),
    datatype: {
      name: "Sprint",
      name_pluralized: "Sprints",
      endpoint: urls.SPRINTS,
      fields: [
        {
          id: "iteration",
          label: "Sprint Iteration",
          numeric: true,
          disablePadding: true
        },
        {
          id: "story",
          label: "Sprint - User Story",
          numeric: true,
          disablePadding: false
        },
        {
          id: "memberAssigned",
          label: "Member Assigned",
          render: false,
          numeric: true,
          disablePadding: false
        },
        {
          id: "estimate",
          label: "Sprint - Relative Estimate",
          type: "number",
          numeric: true,
          disablePadding: false
        },
        {
          id: "date",
          label: "Complete by Date",
          type: "date",
          default: "2019-03-01",
          numeric: true,
          disablePadding: false
        }
      ]
    }
  };

  componentDidMount() {
    this.state.crud.loadAllDocuments(true);
  }

  async refreshData() {
    this.setState({
      projectMembers: await this.state.crud.getAllDocuments(
        {
          endpoint: urls.PROJECTMEMBERS,
          name: "Member",
          name_pluralized: "Members"
        },
        true
      ),
      userStories: await this.state.crud.getAllDocuments(
        {
          endpoint: urls.BACKLOG,
          name: "Backlog Story",
          name_pluralized: "Backlog Stories"
        },
        false
      )
    });
  }

  generateDropDownMemberValues() {
    let items = [];
    for (let index = 0; index < this.state.projectMembers.length; index++) {
      items.push(
        <MenuItem
          id="memberAssigned"
          key={index}
          value={this.state.projectMembers[index].name}
        >
          {this.state.projectMembers[index].name}
        </MenuItem>
      );
    }
    return items;
  }

  generateDropDownStoryValues() {
    let items = [];
    for (let index = 0; index < this.state.userStories.length; index++) {
      items.push(
        <MenuItem
          id="story"
          key={index}
          value={this.state.userStories[index].story}
        >
          {this.state.userStories[index].story}
        </MenuItem>
      );
    }
    return items;
  }

  renderSpecialFields() {
    let specialFields = [];

    specialFields.push(
      <div>
        <Select
          value={this.state.openDocument["story"]}
          onChange={this.state.crud.updateDropDown}
          id="storyItem"
          label="Story Point"
          fullWidth
        >
          {this.generateDropDownStoryValues()}>
        </Select>
        <TextField
          value={this.state.openDocument["estimate"]}
          id="estimateAssigned"
          helperText="Story Estimate"
          numeric
          disabled
        />

        <Select
          value={this.state.openDocument["memberAssigned"]}
          onChange={this.state.crud.updateDropDown}
          defaultValue="0"
          id="memberAssigned"
          label="Member Assigned"
          fullWidth
        >
          {this.generateDropDownMemberValues()}
        </Select>
      </div>
    );

    return specialFields;
  }

  getDocumentRowColor(doc) {
    return "white";
  }

  render() {
    return this.state.crud.render(this.props.classes, false);
  }
}

SprintItemTable.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(Crud.styles)(SprintItemTable);
