/**
 * Project:       Sprint Tool Project
 * School:        Fanshawe College
 * Class:         INFO3112
 * Semester:      Winter 2019
 * Professor:     Admiral Brian Turford
 * Team Members:  Ryan Augustynowicz, Kevin Cox, Nomaan Abbasey
 */

const SERVER = "http://localhost:5000/sprint"; // development
// const SERVER = "/sprint";
//urls.js
//url constants

module.exports = {
  BACKLOG: `${SERVER}/backlog`,
  BACKLOGALL: `${SERVER}/backlog/all`,
  SPRINTS: `${SERVER}/sprints`,
  SPRINTSALL: `${SERVER}/sprints/all`,
  PROJECTMEMBERS: `${SERVER}/members`,
  PROJECTMEMBERSALL: `${SERVER}/members/all`
};
