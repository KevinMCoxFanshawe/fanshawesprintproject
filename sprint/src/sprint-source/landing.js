/**
 * Project:       Sprint Tool Project
 * School:        Fanshawe College
 * Class:         INFO3112
 * Semester:      Winter 2019
 * Professor:     Admiral Brian Turford
 * Team Members:  Ryan Augustynowicz, Kevin Cox, Nomaan Abbasey
 */

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Toolbar,
  AppBar,
  Menu,
  MenuItem,
  IconButton,
  Typography,
  Snackbar
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Reorder from "@material-ui/icons/Reorder";
import BackloggedItemsTable from "./backloggedItemsComponent";
import SprintItemTable from "./sprintComponent";
import MembersComponent from "./members.js";
import Cover from 'react-video-cover';

class ProjectLandingComponent extends React.PureComponent {
  state = {
    onHomeSelected: true,
    anchorEl: null,
    showBackLog: false,
    snackbarMsg: "",
    gotData: false,
    showMembers: false,
    showSprint: false
  };

  onMenuItemClicked = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  onHomeSelectedItemClicked = () => {
    this.setState({
      anchorEl: null,
      showBackLog: false,
      onHomeSelected: true,
      showMembers: false,
      showSprint: false
    });
  };

  onClose = () => {
    this.setState({ anchorEl: null });
  };

  onBacklogClicked = () => {
    this.setState({
      anchorEl: null,
      showBackLog: true,
      onHomeSelected: false,
      showMembers: false,
      showSprint: false
    });
  };

  onSprintClicked = () => {
    this.setState({
      anchorEl: null,
      showBackLog: false,
      onHomeSelected: false,
      showMembers: false,
      showSprint: true
    });
  }

  onMembersClicked = () => {
    this.setState({
      anchorEl: null,
      showBackLog: false,
      onHomeSelected: false,
      showMembers: true,
      showSprint: false
    });
  };


  //////////////////////////

  snackbarClose = () => {
    this.setState({ gotData: false });
  };

  setSnackbarMessage = msg => {
    this.setState({ gotData: true, snackbarMsg: msg });
  };

  render() {
    const {
      anchorEl,
      showBackLog,
      onHomeSelected,
      gotData,
      snackbarMsg,
      showMembers,
      showSprint
    } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <AppBar position="static">
          <Toolbar>
            <IconButton onClick={this.onMenuItemClicked} color="inherit">
              <Reorder />
            </IconButton>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.onClose}
            >
              <MenuItem onClick={this.onHomeSelectedItemClicked}>Home</MenuItem>
              <MenuItem onClick={this.onBacklogClicked}>Backlog</MenuItem>
              <MenuItem onClick={this.onMembersClicked}>Members</MenuItem>
              <MenuItem onClick={this.onSprintClicked}>Sprints</MenuItem>
            </Menu>
            <Typography variant="h6" color="inherit">
              Project - Sprint Tool
            </Typography>
          </Toolbar>
        </AppBar>
        {onHomeSelected && (
          <div className="video">
            <Cover
              videoOptions={{
                src: "./Untitled.mp4", autoPlay: true, muted: true,
                loop: true
              }}
            />
            <div className="landing">
              <h1>
                Sprinting Has Never Been Easier
            </h1>
              <h3>
                Developed by Kevin Cox, Nomaan Abbasey, and Ryan Augustynowicz
            </h3>
            </div>
          </div>
        )}

        {showBackLog && <BackloggedItemsTable loadMessage={this.setSnackbarMessage} />}
        {showMembers && <MembersComponent loadMessage={this.setSnackbarMessage} />}
        {showSprint && <SprintItemTable loadMessage={this.setSnackbarMessage} />}
        <Snackbar
          open={gotData}
          message={snackbarMsg}
          autoHideDuration={4000}
          onClose={this.snackbarClose}
        />
      </MuiThemeProvider>
    );
  }
}

export default ProjectLandingComponent;
