/**
 * Project:       Sprint Tool Project
 * School:        Fanshawe College
 * Class:         INFO3112
 * Semester:      Winter 2019
 * Professor:     Admiral Brian Turford
 * Team Members:  Ryan Augustynowicz, Kevin Cox, Nomaan Abbasey
 */

import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Crud from "./crud";
const urls = require("./urls");

class BackloggedItemsTable extends React.Component {
  state = {
    order: "asc",
    orderBy: "desc",
    selected: [],
    data: [],
    page: 0,
    rowsPerPage: 5,
    deleteDialogVisible: false,
    openDocument: {},
    updateDialogVisible: false,
    crud: new Crud(this),
    completeRowColor: "",
    velocity: 0,
    datatype: {
      name: "Back Log Item",
      name_pluralized: "Back Log Items",
      endpoint: urls.BACKLOG,
      fields: [
        {
          id: "name",
          label: "Product Feature",
          numeric: false,
          disablePadding: true
        },
        {
          id: "story",
          label: "I Want To...",
          numeric: true,
          disablePadding: false
        },
        {
          id: "desc",
          label: "So That I Can...",
          numeric: true,
          disablePadding: false
        },
        {
          id: "priority",
          label: "Priority",
          type: "number",
          numeric: true,
          disablePadding: false
        },
        {
          id: "estimate",
          label: "Relative Estimate",
          render: false,
          numeric: true,
          disablePadding: false
        },
        { id: "role", label: "Role", numeric: true, disablePadding: false },
        {
          id: "completion_percentage",
          label: "Percent Completed",
          type: "number",
          default: 0,
          numeric: true,
          disablePadding: false
        }
      ]
    }
  };

  componentDidMount() {
    this.state.crud.loadAllDocuments(true);
  }

  renderSpecialFields() {}

  getDocumentRowColor(doc) {
    return doc["completion_percentage"] >= 100 ? "green" : "white";
  }

  render() {
    return this.state.crud.render(this.props.classes, true);
  }
}

BackloggedItemsTable.propTypes = { classes: PropTypes.object.isRequired };

export default withStyles(Crud.styles)(BackloggedItemsTable);
