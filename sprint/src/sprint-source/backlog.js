/**
 * Project:       Sprint Tool Project
 * School:        Fanshawe College
 * Class:         INFO3112
 * Semester:      Winter 2019
 * Professor:     Admiral Brian Turford
 * Team Members:  Ryan Augustynowicz, Kevin Cox, Nomaan Abbasey
 */

import React, { PureComponent } from 'react';
import {
    SortableContainer,
    SortableElement,
    arrayMove,
} from 'react-sortable-hoc';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { Card, CardHeader, CardContent, Typography, Paper } from '@material-ui/core';
import {
    Table,
    TableBody,
    TableRow,
    TableCell,
    TableHead
} from '@material-ui/core';
import theme from './theme';
import '../App.css';

const urls = require('./urls');

const SortableItem = SortableElement(({ value, rowIndex }) => {
    return (<TableRow key={rowIndex}>
        {value.map((col, index) => (
            <TableCell key={index}>{col}</TableCell>
        ))}
    </TableRow>)
});

const SortableList = SortableContainer(({ items }) => {
    return (
        <TableBody>
            {items.map((value, index) => (
                <SortableItem key={`item-${index}`} index={index} value={value} rowIndex={index} />
            ))}
        </TableBody>
    );
});

class SortableComponent extends PureComponent {
    state = {
        items: [['Item 1', 'Col1'], ['Item 2', 'col2'], ['Item 3', 'col3']],
    };
    onSortEnd = ({ oldIndex, newIndex }) => {
        this.setState(({ items }) => ({
            items: arrayMove(items, oldIndex, newIndex),
        }));
    };

    async componentDidMount() {

    }
    render() {
        return (
            <MuiThemeProvider theme={theme}>

                <Card style={{ marginTop: '2%' }}>

                    <Paper>
                        <Typography
                            color="primary"
                            style={{ textAlign: 'center', verticalAlign: 'middle' }}
                        >
                            Backloged Tasks
                        </Typography>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Backloged Item</TableCell>
                                    <TableCell>Desc.</TableCell>
                                    <TableCell>Edit</TableCell>
                                </TableRow>
                            </TableHead>

                            <SortableList items={this.state.items} onSortEnd={this.onSortEnd} />
                        </Table>
                    </Paper>
                </Card>
            </MuiThemeProvider >);
    }
}
export default SortableComponent;