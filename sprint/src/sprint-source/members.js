/**
 * Project:       Sprint Tool Project
 * School:        Fanshawe College
 * Class:         INFO3112
 * Semester:      Winter 2019
 * Professor:     Admiral Brian Turford
 * Team Members:  Ryan Augustynowicz, Kevin Cox, Nomaan Abbasey
 */

import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Crud from "./crud";
const urls = require("./urls");

class MembersComponent extends React.Component {
  state = {
    order: "asc",
    orderBy: "desc",
    selected: [],
    data: [],
    page: 0,
    rowsPerPage: 5,
    deleteDialogVisible: false,
    openDocument: {},
    updateDialogVisible: false,
    crud: new Crud(this),
    datatype: {
      name: "Member",
      name_pluralized: "Members",
      endpoint: urls.PROJECTMEMBERS,
      fields: [
        { id: "name", label: "Name", numeric: true, disablePadding: true },
        {
          id: "email",
          label: "Email Address",
          numeric: true,
          disablePadding: false
        },
        { id: "status", label: "Status", numeric: true, disablePadding: false }
      ]
    }
  };

  componentDidMount() {
    this.state.crud.loadAllDocuments(true);
  }

  renderSpecialFields() { }

  getDocumentRowColor(doc) {
    return "white";
  }

  render() {
    return this.state.crud.render(this.props.classes, false);
  }
}

MembersComponent.propTypes = { classes: PropTypes.object.isRequired };

export default withStyles(Crud.styles)(MembersComponent);
