/**
 * Project:       Sprint Tool Project
 * School:        Fanshawe College
 * Class:         INFO3112
 * Semester:      Winter 2019
 * Professor:     Admiral Brian Turford
 * Team Members:  Ryan Augustynowicz, Kevin Cox, Nomaan Abbasey
 */

require('dotenv').config();
const express = require("express");
const router = express.Router();
const dbRoutines = require("./dbroutines");
const mongoClient = require("mongodb").MongoClient;

// ADD NEW DATA TYPES HERE
const datatypes = [
  { endpoint: "/members", collection: process.env.MEMBERCOLLECTION },
  { endpoint: "/sprints", collection: process.env.SPRINTSCOLLECTION },
  { endpoint: "/backlog", collection: process.env.PRODUCTBACKLOGCOLLECTION }
]

datatypes.forEach(datatype => {
  var endpoint = datatype["endpoint"];
  var collection = datatype["collection"];
  
  //get all documents
  router.get(endpoint + "/all", async (req, res) => {
    let conn;
    try {
      conn = await mongoClient.connect(process.env.DBURL, {
        useNewUrlParser: true
      });
      const db = conn.db(process.env.DB);
      let documents = await dbRoutines.getAll(db, collection);
      res.send(documents);
    } catch (err) {
      console.log(err.stack);
      res.status(500).send("get all documents failed - internal server error");
    } finally {
      if (conn) conn.close();
    }
  });

  //get by document by id
  router.get(endpoint + "/:id", async (req, res) => {
    let conn;
    try {
      conn = await mongoClient.connect(process.env.DBURL, {
        useNewUrlParser: true
      });
      const db = conn.db(process.env.DB);
      let id = req.params.id;

      let document = await dbRoutines.findById(
        id,
        db,
        collection
      );
      res.send(document);
    } catch (err) {
      console.log(err.stack);
      res.status(500).send("get document failed - internal server error");
    } finally {
      if (conn) conn.close();
    }
  });

  // create document
  router.put(endpoint, async (req, res) => {
    let conn;
    try {
      conn = await mongoClient.connect(process.env.DBURL, {
        useNewUrlParser: true
      });
      const db = conn.db(process.env.DB);

      //put in new data into object from req.body
      let result = await dbRoutines.addOneDoc(
        req.body,
        db,
        collection
      );

      let response = `${result.insertedCount} document(s) were created`;
      let msg = await dbRoutines.setResponse(response);
      res.send(msg);
    } catch (err) {
      console.log(err);
      res.status(500).send("Create failed");
    } finally {
      if (conn) conn.close();
    }
  });

  // update document
  router.post(endpoint, async (req, res) => {
    let conn;
    try {
      conn = await mongoClient.connect(process.env.DBURL, {
        useNewUrlParser: true
      });
      const db = conn.db(process.env.DB);

      let updateResult = await dbRoutines.updateOne(
        req.body,
        db,
        collection
      );

      let msg = await dbRoutines.setResponse(
        `Updated ${updateResult.modifiedCount} document(s)`
      );

      res.send(msg);
    } catch (err) {
      console.log(err.stack);
      res.status(500).send("Error - Unable to update document");
    } finally {
      if (conn) conn.close();
    }
  });

  //delete document based on id
  router.delete(endpoint + "/:id", async (req, res) => {
    let conn;
    try {
      conn = await mongoClient.connect(process.env.DBURL, {
        useNewUrlParser: true
      });
      const db = conn.db(process.env.DB);
      let itemid = req.params.id;
      let result = await dbRoutines.deleteOne(
        itemid,
        db,
        collection
      );
      let response = `${result.deletedCount} document(s) were deleted`;
      let msg = await dbRoutines.setResponse(response);
      res.send(msg);
    } catch (err) {
      console.log(err.stack);
      res.status(500).send("delete failed");
    } finally {
      if (conn) conn.close();
    }
  });

});

module.exports = router;