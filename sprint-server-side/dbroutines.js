/**
 * Project:       Sprint Tool Project
 * School:        Fanshawe College
 * Class:         INFO3112
 * Semester:      Winter 2019
 * Professor:     Admiral Brian Turford
 * Team Members:  Ryan Augustynowicz, Kevin Cox, Nomaan Abbasey
 */

var ObjectID = require('mongodb').ObjectID;

// delete all documents from collection
const deleteAll = (db, coll) => db.collection(coll).deleteMany({});

//get all
const getAll = (db, coll) =>
  db
    .collection(coll)
    .find()
    .toArray();

//add one json object to the specified collection
const addOneDoc = (doc, db, coll) => db.collection(coll).insertOne(doc);

//get product based on id
const findById = (id, db, coll) => {
  var o_id = new ObjectID(id);
  return db
    .collection(coll)
    .find(o_id)
    .toArray();
};

//update any object in database
const updateOne = (item, db, coll) => {

  var id = ObjectID(item['_id']);
  delete item['_id']


  return db.collection(coll).updateOne(
    { "_id": id },
    { $set: item }
  );
};

//delete any one object in database
const deleteOne = (id, db, coll) => {

  return db.collection(coll).deleteOne({ "_id": ObjectID(id) });
};

const setResponse = msg => {
  return new Promise((resolve, reject) => {
    resolve({
      // with property value shorthand syntax, you can omit the property
      // value if key matches variable name. So this is just a short form of
      // resolve({name: name, age: age, email: email})
      msg
    });
  });
};

//export all these methods
module.exports = {
  deleteAll,
  addOneDoc,
  getAll,
  findById,
  updateOne,
  deleteOne,
  setResponse
};
