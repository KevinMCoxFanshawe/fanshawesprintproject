/**
 * Project:       Sprint Tool Project
 * School:        Fanshawe College
 * Class:         INFO3112
 * Semester:      Winter 2019
 * Professor:     Admiral Brian Turford
 * Team Members:  Ryan Augustynowicz, Kevin Cox, Nomaan Abbasey
 */

require("dotenv").config();
//app.js
//"main" file

const bodyParser = require("body-parser");
const express = require("express");
const standardRoutes = require("./standardRoutes");
const app = express();
const port = process.env.PORT || 5000;

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE");
  next();
});

// parse application/json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//use the routes we created
app.use("/sprint", standardRoutes);

app.use(express.static("public"));
//open sever on port 5000
app.listen(port, () => {
  console.log(`Server started on port - ${process.env.PORT}`);
});
